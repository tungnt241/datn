import pandas as pd
import tensorflow as tf
import numpy as np
from keras.layers import TextVectorization
import joblib
import os

fileTrain = '.\\data\\result.csv'
fileTest = '.\\data\\translated_test.csv'
file_vector = '.\\models\\vectorize\\vectorizer.joblib'

MAX_FEATURES = 200000

# df = pd.read_csv(fileTest)

# X = df['comment_text']
# y = df[df.columns[2:]].values

# if os.path.exists(file_vector):
#     loaded_vectorizer = joblib.load(file_vector)
# else:
#     print(f"Không tìm thấy tệp: {file_vector}")

# loaded_vectorizer = joblib.load('.\\models\\vectorize\\vectorizer.joblib')
# model = tf.keras.models.load_model('.\\models\\toxicity_1.hdf5')

# for i in range(0, 10):
#     vectorized_comment = loaded_vectorizer([X[i]])
#     res = model.predict(vectorized_comment)
#     result_matrix = list(map(lambda x: 1 if x > 0.6 else 0, res[0]))
#     print(result_matrix)

df = pd.read_csv(fileTrain)

X = df['comment_text'].astype(str)
y = df[df.columns[2:]].values

print(X.head)