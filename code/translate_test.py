import os
import pandas as pd
from google.cloud import translate_v2 as translate

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"cloudcomputing-298809-1ab611237bd4.json"
translate_client = translate.Client()


filename = 'refactor_test.csv'
target = "vi"

# Đọc file CSV gốc
df = pd.read_csv(filename)

def custom_processing(text : str):
    result = {}
    if text and not pd.isna(text):
        try: 
            result = translate_client.translate(text, target_language=target)
        except Exception as e:
            print(f"Error processing text: {text}")
            print(e)
        return result.get('translatedText')
    return 'noi dung loi'

df['comment_text'] = df['comment_text'].apply(custom_processing)

# Lưu thành file CSV mới
df.to_csv('translated_test.csv', index=False, encoding='utf-8-sig')

