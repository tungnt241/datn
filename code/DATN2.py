import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB

fileTrain = 'translate_train.csv'
fileTest = '.\hihihi\\test.csv'
fileTestLabel = '.\hihihi\\test_labels.csv'

# Load dữ liệu train từ file CSV
train_data = pd.read_csv(fileTrain)

# Xử lý giá trị thiếu
train_data['comment_text'].fillna('', inplace=True)

# Chia dữ liệu thành tập huấn luyện
X_train, y_train = train_data['comment_text'], train_data[['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']]

# Sử dụng Bag of Words để biểu diễn văn bản thành ma trận
vectorizer = CountVectorizer()
X_train_vectorized = vectorizer.fit_transform(X_train)

# Huấn luyện mô hình Naive Bayes
models = {}
for label in y_train.columns:
    model = MultinomialNB()
    model.fit(X_train_vectorized, y_train[label])
    models[label] = model

# Nhập nội dung mới từ console
new_content = input("Nhập nội dung mới: ")

# Biểu diễn nội dung mới thành ma trận
new_content_vectorized = vectorizer.transform([new_content])

# Dự đoán
predictions = {}
for label, model in models.items():
    predictions[label] = model.predict(new_content_vectorized)

result = False
for label in predictions:
    # In kết quả
    if predictions[label] == 1:
        result = True
        print(predictions[label])
    else:
        print(predictions[label])

print(result)