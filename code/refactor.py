import pandas as pd
import re
from underthesea import word_tokenize


fileTrain = '.\\data\\translate_train.csv'
fileTest = '.\\data\\translated_test.csv'
fileTestLabel = '.\\data\\test_labels.csv'


def remove_html(txt):
    if pd.isna(txt):
        return txt
    return re.sub(r"<[^>]+>", "", str(txt)).strip()


def low_doc(txt):
    if pd.isna(txt):
        return txt
    return txt.lower()


def text_preprocess(document: str):
    # xóa html code
    document = remove_html(document)

    # đưa về lower
    document = low_doc(document)

    # xóa các ký tự không cần thiết
    document = re.sub(r"[^\s\wáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệóòỏõọôốồổỗộơớờởỡợíìỉĩịúùủũụưứừửữựýỳỷỹỵđ_]",' ',document).strip()
    # xóa khoảng trắng thừa
    document = re.sub(r"\s+|\\&quot;|\\&|\\#\\@\\:\\[\\]|==|quot", ' ', document).strip()
    document = re.sub(r"\s+", " ", document)
    return document

# Đọc dữ liệu từ các file CSV
file1 = pd.read_csv(fileTrain, dtype={"comment_text": str})
file2 = pd.read_csv(fileTest, dtype={"comment_text": str})
file3 = pd.read_csv(fileTestLabel)

# Ghép file2 và file3 theo cột 'id'
merged_file = pd.merge(file2, file3, on='id', how='inner')

# Sắp xếp lại các cột theo thứ tự như file1
merged_file = merged_file[['id', 'comment_text', 'toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']]

# Nối dữ liệu vào file1
final_result = pd.concat([file1, merged_file])

final_result['comment_text'] = final_result['comment_text'].apply(lambda x: str(x))
final_result['comment_text'] = final_result['comment_text'].apply(text_preprocess)

final_result[['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']] = final_result[['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']].map(lambda x: 0 if x == -1 else x)
final_result['comment_text'] = final_result['comment_text'].apply(lambda x: str(x))
print(final_result.head)

# Lưu kết quả vào file mới
final_result.to_csv('.\\data\\result2.csv', index=False, encoding='utf-8-sig')
