from bs4 import BeautifulSoup
import time
import datetime
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.chrome.service import Service
import undetected_chromedriver as uc


api_url = 'https://voz.vn'
driver = uc.Chrome()

topic = ['https://voz.vn/f/gop-y.3/', 'https://voz.vn/s/tin-tuc-inet.102/', 'https://voz.vn/s/review-san-pham.103/', 'https://voz.vn/s/chia-se-kien-thuc.104/',
         'https://voz.vn/f/tu-van-cau-hinh.70/', 'https://voz.vn/f/overclocking-cooling-modding.6/', 'https://voz.vn/f/amd.25/', 'https://voz.vn/f/intel.24/',
         'https://voz.vn/f/gpu-man-hinh.8/', 'https://voz.vn/f/phan-cung-chung.9/', 'https://voz.vn/f/thiet-bi-ngoai-vi-phu-kien-mang.30/',
         'https://voz.vn/f/server-nas-render-farm.83/', 'https://voz.vn/f/small-form-factor-pc.61/', 'https://voz.vn/f/hackintosh.62/',
         'https://voz.vn/f/may-tinh-xach-tay.47/', 'https://voz.vn/f/phan-mem.13/', 'https://voz.vn/f/app-di-dong.21/', 'https://voz.vn/f/pc-gaming.11/',
         'https://voz.vn/f/console-gaming.22/', 'https://voz.vn/f/android.32/', 'https://voz.vn/f/apple.36/', 'https://voz.vn/f/multimedia.31/',
         'https://voz.vn/f/do-dien-tu-thiet-bi-gia-dung.10/', 'https://voz.vn/f/chup-anh-quay-phim.75/', 'https://voz.vn/f/goc-chien-luoc.101/',
         'https://voz.vn/f/tuyen-dung-tim-viec.95/', 'https://voz.vn/f/ngoai-ngu.90/', 'https://voz.vn/f/lap-trinh-cntt.91/', 'https://voz.vn/f/kinh-te-luat.92/',
         'https://voz.vn/f/make-money-online.93/', 'https://voz.vn/f/tien-dien-tu.94/', 'https://voz.vn/f/chuyen-tro-linh-tinh.17/', 'https://voz.vn/f/diem-bao.33/',
         'https://voz.vn/f/4-banh.38/', 'https://voz.vn/f/2-banh.39/', 'https://voz.vn/f/the-duc-the-thao.63/', 'https://voz.vn/f/am-thuc-du-lich.64/',
         'https://voz.vn/f/phim-nhac-sach.65/', 'https://voz.vn/f/thoi-trang-lam-dep.66/', 'https://voz.vn/f/cac-thu-choi-khac.67/', 'https://voz.vn/f/may-tinh-de-ban.68/',
         'https://voz.vn/f/may-tinh-xach-tay.72/', 'https://voz.vn/f/dien-thoai-di-dong.76/', 'https://voz.vn/f/xe-cac-loai.77/', 'https://voz.vn/f/thoi-trang-lam-dep.78/',
         'https://voz.vn/f/bat-dong-san.79/', 'https://voz.vn/f/an-uong-du-lich.81/', 'https://voz.vn/f/sim-so-do-phong-thuy.82/',  'https://voz.vn/f/san-pham-dich-vu-khac.80/']

# scraper = cfscrape.create_scraper()

# Lấy dữ liệu
# a = datetime.datetime.now()
# driver.get(api_url)
# try: 
#     time.sleep(2)
# except:
#     print("error")

# data = driver.page_source
# # Sử dụng BeautifulSoup để phân tích HTML
# soup = BeautifulSoup(data, 'html.parser')

# topic = []

# # Tìm tất cả các thẻ div có class 'block-body'
# block_body_divs = soup.find_all('div', class_='block-body')


# # Lặp qua từng div có class 'block-body'
# for block_body_div in block_body_divs:
#     # Tìm tất cả các thẻ div có class 'node-main js-nodeMain'
#     node_main_divs = block_body_div.find_all('div', class_='node-main js-nodeMain')

#     # Lặp qua từng div có class 'node-main js-nodeMain' trong div có class 'block-body'N
#     for node_main_div in node_main_divs:
#         # Tìm thẻ h3 bên trong div 'node-main js-nodeMain'
#         h3_tag = node_main_div.find('h3')

#         # Kiểm tra xem thẻ h3 có thuộc tính 'href' hay không
#         if h3_tag and 'href' in h3_tag.a.attrs:
#             topic.append(api_url + h3_tag.a['href'])


# # b = datetime.datetime.now()     
   
thread = []

count = 0

# for url in topic:

#     driver.get(url)
#     try: 
#         time.sleep(2)
#     except:
#         print("error")
#     data = driver.page_source

#     soup2 = BeautifulSoup(data, 'html.parser')

#     # Tìm tất cả các thẻ div có class 'structItem-cell structItem-cell--main'
#     block_body_divs2 = soup2.find_all('div', class_='structItem-cell structItem-cell--main')

#     # Lặp qua từng div có class 'structItem-cell structItem-cell--main'
#     for block_body_div in block_body_divs2:
#         # Tìm tất cả các thẻ div có class 'structItem-title'
#         node_main_divs = block_body_div.find_all('div', class_='structItem-title')
#         # Lặp qua từng div có class 'structItem-title' trong div có class 'structItem-cell structItem-cell--main'
#         for node_main_div in node_main_divs:
#             # Tìm thẻ a cuối cùng bên trong div 'structItem-title'
#             a_tag = node_main_div.find_all('a')
#             # Kiểm tra xem thẻ a có thuộc tính 'href' hay không
            
#             if a_tag[-1] and 'href' in a_tag[-1].attrs:
#                 thread.append(api_url + a_tag[-1]['href'])
#                 count+=1
# b = datetime.datetime.now()  

thread = ['https://voz.vn/t/nguoi-ha-noi-nhich-tung-met-vuot-un-tac-trong-mua-ret.884591/page-2']   

a = datetime.datetime.now()  
for url in thread:
    x = datetime.datetime.now() 
    driver.get(url)
    time.sleep(1)
    data = driver.page_source
    soup3 = BeautifulSoup(data, 'html.parser')
    # y = datetime.datetime.now() 
    # print('time load')
    # print(y - x)
    # c = datetime.datetime.now()
    block_body_divs3 = soup3.find_all('article', class_='message message--post js-post js-inlineModContainer')
    c = datetime.datetime.now() 
    for block_body_div in block_body_divs3:
        # Tìm thẻ div có class 'bbWrapper'
        bb_wrapper_div = block_body_div.find('div', class_='bbWrapper')
        if bb_wrapper_div and not bb_wrapper_div.find('blockquote'):
            print(bb_wrapper_div.text.strip())
        else:
            for blockquote in bb_wrapper_div.find_all('blockquote'):
                blockquote.decompose()  # Loại bỏ thẻ blockquote
            text_content = " ".join(bb_wrapper_div.stripped_strings)
            print(text_content)
    # d = datetime.datetime.now()
    # print('time search')
    # print(d - c)

b = datetime.datetime.now()  
print(b - a)
driver.quit()
