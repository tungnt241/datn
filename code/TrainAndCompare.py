import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score, classification_report
import tensorflowjs as tfjs
from keras.models import load_model
import joblib

fileTrain = '.\data\\translate_train.csv'
fileTest = '.\data\\translated_test.csv'
fileTestLabel = '.\hihihi\\test_labels.csv'
resultFile ='.\hihihi\\result_calculator.csv'

# Load dữ liệu train từ file CSV
train_data = pd.read_csv(fileTrain)
# Xử lý giá trị thiếu
train_data['comment_text'].fillna('', inplace=True)

labels = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']

# Chia dữ liệu thành tập huấn luyện và tập kiểm thử
X_train, X_test, y_train = train_data['comment_text'], train_data[labels], train_data[labels]

# Chuyển giá trị -1 về 0
y_train = y_train.map(lambda x: 0 if x == -1 else x)

# Sử dụng Bag of Words để biểu diễn văn bản thành ma trận
vectorizer = CountVectorizer()
X_train_vectorized = vectorizer.fit_transform(X_train)

# Huấn luyện mô hình Naive Bayes cho từng label
models = {}
for label in y_train.columns:
    model = MultinomialNB()
    model.fit(X_train_vectorized, y_train[label])
    models[label] = model

# Load dữ liệu test từ file CSV
test_data = pd.read_csv(fileTest)
test_data['comment_text'].fillna('', inplace=True)

# Biểu diễn văn bản thành ma trận sử dụng vectorizer đã được huấn luyện
X_test_data = vectorizer.transform(test_data['comment_text'])

# Dự đoán
predictions = {}
for label, model in models.items():
    predictions[label] = model.predict(X_test_data)

# Tạo DataFrame kết quả
result_df = pd.DataFrame({'id': test_data['id'], 'toxic': predictions['toxic'], 'severe_toxic': predictions['severe_toxic'],
                           'obscene': predictions['obscene'], 'threat': predictions['threat'],
                           'insult': predictions['insult'], 'identity_hate': predictions['identity_hate']})

result_df['is_not_good'] = (result_df[labels].sum(axis=1) > 0).astype(int)    

# Load dữ liệu test_label từ file CSV
test_label_data = pd.read_csv(fileTestLabel)
test_label_data['is_not_good'] = (test_label_data[labels].sum(axis=1) > 0).astype(int)   

# Tính độ chính xác
accuracy_label1 = accuracy_score(test_label_data['is_not_good'], result_df['is_not_good'])

print(accuracy_label1)

#Lưu trữ model
for label, model in models.items():
    tfjs.converters.save_keras_model(model, f'path/to/tfjs/{label}_model')

# Lưu trữ vectorizer
joblib.dump(vectorizer, 'path/to/tfjs/vectorizer.joblib')