import os
import re
import pandas as pd
import tensorflow as tf
# from tensorflow.python.keras.layers import Dense
# from tensorflow import keras
from keras.layers import Dense
from keras.models import Sequential, load_model
from keras.layers import TextVectorization
import numpy as np
from keras.models import Sequential
from keras.layers import LSTM, Dropout, Bidirectional, Dense, Embedding
# from matplotlib import pyplot as plt
from keras.callbacks import ModelCheckpoint
from keras.metrics import Precision, Recall, CategoricalAccuracy
import joblib
import pickle

fileTrain = '.\\data\\translate_train.csv'
fileTest = '.\\data\\test.csv'
fileTestLabel = '.\\data\\test_labels.csv'

MAX_FEATURES = 200000

def remove_html(txt):
    if pd.isna(txt):
        return txt
    return re.sub(r"<[^>]+>", "", str(txt)).strip()


def low_doc(txt):
    if pd.isna(txt):
        return txt
    return txt.lower()


def text_preprocess(document: str):
    # xóa html code
    document = remove_html(document)

    # đưa về lower
    document = low_doc(document)

    # xóa các ký tự không cần thiết
    document = re.sub(r"[^\s\wáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệóòỏõọôốồổỗộơớờởỡợíìỉĩịúùủũụưứừửữựýỳỷỹỵđ_]",' ',document).strip()
    # xóa khoảng trắng thừa
    document = re.sub(r"\s+|\\&quot;|\\&|\\#\\@\\:\\[\\]|==|quot", ' ', document).strip()
    document = re.sub(r"\s+", " ", document)
    return document



df = pd.read_csv(fileTrain)

X = df['comment_text'].astype(str)
y = df[df.columns[2:]].values


# print(X.head)

vectorizer = TextVectorization(max_tokens=MAX_FEATURES,
                               output_sequence_length=4000,
                               output_mode='int')
vectorizer.adapt(X.values)

# Vector for word "this"
print (vectorizer(X.values))
# Pickle the config and weights
pickle.dump({'config': vectorizer.get_config(),
             'weights': vectorizer.get_weights()}
            , open("tv_layer_2.pkl", "wb"))
# # print ("*"*10)
# # Later you can unpickle and use 
# # `config` to create object and 
# # `weights` to load the trained weights. 
# from_disk = pickle.load(open("tv_layer.pkl", "rb"))
# vector_fromd_disk = TextVectorization.from_config(from_disk['config'])
# You have to call `adapt` with some dummy data (BUG in Keras)
# vector_fromd_disk.adapt(tf.data.Dataset.from_tensor_slices(["xyz"]))
# vector_fromd_disk.set_weights(from_disk['weights'])
# Lets see the Vector for word "this"
# # print (vector_fromd_disk(X.values))

vectorized_text = vectorizer(X.values)

#MCSHBAP - map, chache, shuffle, batch, prefetch  from_tensor_slices, list_file
dataset = tf.data.Dataset.from_tensor_slices((vectorized_text, y))
dataset = dataset.cache()
dataset = dataset.shuffle(160000)
dataset = dataset.batch(16)
dataset = dataset.prefetch(8) # helps bottlenecks

train = dataset.take(int(len(dataset)*.7))
val = dataset.skip(int(len(dataset)*.7)).take(int(len(dataset)*.2))
test = dataset.skip(int(len(dataset)*.9)).take(int(len(dataset)*.1))

model = Sequential()
# Create the embedding layer 
model.add(Embedding(MAX_FEATURES+1, 32))
# Bidirectional LSTM Layer
model.add(Bidirectional(LSTM(32, activation='tanh')))
# Feature extractor Fully connected layers
model.add(Dense(6, activation='sigmoid'))
model.add(Dense(32, activation='relu'))
model.add(Dense(64, activation='relu'))
model.add(Dense(128, activation='relu'))
model.add(Dense(256, activation='relu'))
model.add(Dense(512, activation='relu'))
model.add(Dense(1024, activation='relu'))
model.add(Dense(512, activation='relu'))
model.add(Dense(256, activation='relu'))
model.add(Dense(128, activation='relu'))
model.add(Dense(64, activation='relu'))
model.add(Dense(32, activation='relu'))
# Final layer 
model.add(Dense(6, activation='sigmoid'))

model.compile(loss='BinaryCrossentropy', optimizer='Adam')
checkpoint = ModelCheckpoint('.\\models\\best_2.hdf5', save_best_only=True, monitor = 'val_loss', mode='auto')
callback_list = [checkpoint]
model.summary()
model.fit(train, epochs=3, validation_data=val, callbacks=callback_list)

# # plt.figure(figsize=(8,5))
# # pd.DataFrame(history.history).plot()
# # plt.show()

# # input_text = vectorizer('mẹ thằng chó này.')
 
# # res = model.predict(np.expand_dims(input_text, 0))
# # (res > 0.5).astype(int)
# # batch_X, batch_y = test.as_numpy_iterator().next()
# # (model.predict(batch_X) > 0.5).astype(int)
# # res.shape

pre = Precision()
re = Recall()
acc = CategoricalAccuracy()

for batch in test.as_numpy_iterator(): 
    # Unpack the batch 
    X_true, y_true = batch
    # Make a prediction 
    yhat = model.predict(X_true)
    
    # Flatten the predictions
    y_true = y_true.flatten()
    yhat = yhat.flatten()
    
    pre.update_state(y_true, yhat)
    re.update_state(y_true, yhat)
    acc.update_state(y_true, yhat)
print(f'Precision: {pre.result().numpy()}, Recall:{re.result().numpy()}, Accuracy:{acc.result().numpy()}')

# model.save('.\\models\\tfjs\\toxicity.hdf5')



# inputText = vector_fromd_disk(text_preprocess('địt mẹ mày, mày là một thằng khốn nạn'))
# new_model = tf.keras.models.load_model('.\\models\\toxicity_1.hdf5')

# res = new_model.predict(np.expand_dims(inputText, 0))
# print(res)
# result = np.where(res[0] > 0.6 , 1, 0)
# print(result)

